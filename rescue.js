#!/usr/bin/env node
(function() {
  'use strict';
  var arr, ii, kk, my, rqd;
  console.printd = console.log;
  process.GLOBAL_KEYS = process.GLOBAL_KEYS || Object.keys(global).sort();
  process.MODULE_TINYJS = process.MODULE_TINYJS || module;
  global.window = global.window || global; my = window.my = window.my || {};
  rqd = window.rqd = window.rqd || {};
  my.INIT = my.INIT || 1;
  //// versioning similar to http://semver.org syntax
  my.VERSION = new Date().toISOString().replace(/\W/g, '.').replace('T', '-');
  //// initialization 1
  if(my.INIT > 1) {return;} my.INIT += 1;
  console.log('process.argv ' + JSON.stringify(process.argv));
  console.log('nodejs', process.version);
  console.log('module', module.filename);
  //// require modules
  arr = [
    'child_process', 'console', 'crypto',
    'fs',
    'http', 'https',
    'net',
    'os',
    'path',
    'querystring',
    'repl', 'stream',
    'url', 'util',
    'vm',
    'zlib'
  ];
  for(ii = 0; ii < arr.length; ii += 1) {kk = arr[ii]; rqd[kk] = rqd[kk] || require(kk);}
  process.require = process.require || require;
  my.FS_CWD = my.FS_CWD || rqd.path.resolve();
  my.FS_MODULE = my.FS_MODULE || rqd.path.dirname(process.MODULE_TINYJS.filename);
  my.IS_NODEJS = my.IS_NODEJS || (process.versions && process.versions.node);
  my.IS_DEVELOPMENT = true;
  my.IS_PRODUCTION = null;
  my.SRC = my.SRC || {};
  //// legacy code
  my.IS_LEGACY = process.version < 'v0.8.0';
  if(my.IS_LEGACY) {
    rqd.fs.exists = rqd.fs.exists || rqd.path.exists;
    rqd.fs.existsSync = rqd.fs.existsSync || rqd.path.existsSync;
  }
  //// my.BUILD_SELF
  try {
    if(/^\x23\x23\x23\x23 tinyjs\/source.js2\n/.test(rqd.fs.readFileSync('source.js2', 'utf8')))
    {my.BUILD_SELF = true;}
  }
  catch(errBuildSelf) {}
  //// rescue from a broken build
  arr = process.argv; my.BUILD_RESCUE = true;
  if(my.BUILD_RESCUE) {
    my.BUILD_SELF = true;
    //// JSLINT_IGNORE_BEG
    eval(
      /[\S\s]+ SRC\x5fBEG tinyjs\/extra.js/.exec(
        rqd.fs.readFileSync('source.js2')
      )[0].
      replace(/^#/gm, '//// #').replace(/\{\{my.SRC_PRINT_D\}\}/g, 'print' + 'd')
    );
    //// JSLINT_IGNORE_END
    my.EXIT = true; process.exit();
  }
  //// restricted filesystem and execution environment
  try {
    arr = process.argv; rqd.fs.statSync('.');
    for(ii = 0; ii < arr.length; ii += 1) {if(arr[ii] === '--envRestricted') {throw true;}}
  }
  catch(errEnvRestricted) {
    my.BUILD_RESCUE = null;
    my.BUILD_SELF = null;
    my.ENV_RESTRICTED = true;
    rqd.child_process = null;
    rqd.fs = null;
  }
} ());