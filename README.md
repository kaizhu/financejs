tinyjs
======
tinyjs is a nodejs micro-framework for creating embedded webservers.  it minifies the server & data (css, html, js, png, ...) into a standalone nodejs script making it suitable for disk-less, embedded systems.

usage example:
--------------
    $ mkdir embeddedServer
    $ cd embeddedServer
    $ ## install tinyjs to ./node_modules
    $ npm install tinyjs
    $ ## we will use the included example.js2 as our server
    $ ln -s ./node_modules/tinyjs/example.js2 .
    $ ## start tinyjs framework
    $ ./node_modules/tinyjs/tinyjs.js --dev

    nodejs v0.8.8
    ["starting repl interpreter ..."]
    > //// start server on port 1337
    > svr.start(1337)
    > //// now you can browse the example server @ http://localhost:1337/client ...

    > //// minify our server into a single file (./build/index.min.js)
    > svr.build({'production': true})
    > //// exit nodejs and go back to shell
    > process.exit()

    $ ## run embedded server
    $ node ./build/index.min.js

    starting repl...
    nodejs v0.8.8
    > ////  browse embedded production server @ http://localhost:1338/client ...
